#from itertools import imap

from django.utils.translation import ugettext_lazy as _
from django.db import models


class State(models.Model):
    name = models.CharField(_('name'), max_length=140, unique=True)

    def __unicode__(self):
        return self.name


class Municipality(models.Model):
    name = models.CharField(_('name'), max_length=140)
    state = models.ForeignKey('State')

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = ('name', 'state')


class PostalCode(models.Model):
    number = models.IntegerField(_('name'), primary_key=True)

    def __unicode__(self):
        return unicode(self.number)


class SettlementType(models.Model):
    name = models.CharField(_('name'), max_length=140, unique=True)

    def __unicode__(self):
        return self.name


class Zone(models.Model):
    name = models.CharField(_('name'), max_length=140, unique=True)

    def __unicode__(self):
        return self.name


class Settlement(models.Model):
    municipality = models.ForeignKey('Municipality')
    postal_code = models.ForeignKey('PostalCode')
    type = models.ForeignKey('SettlementType')
    zone = models.ForeignKey('Zone')
    name = models.CharField(_('name'), max_length=140)

    def __unicode__(self):
        return self.name

    class Meta:
        unique_together = ('municipality', 'postal_code',
                           'type', 'zone', 'name')
