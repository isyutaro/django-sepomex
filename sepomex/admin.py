from django.contrib import admin
from models import State, Municipality, PostalCode, SettlementType
from models import Zone, Settlement


admin.site.register(State)
admin.site.register(Municipality)
admin.site.register(PostalCode)
admin.site.register(SettlementType)
admin.site.register(Zone)
admin.site.register(Settlement)
