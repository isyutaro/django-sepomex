from tastypie.resources import ModelResource, ALL_WITH_RELATIONS
from tastypie import fields

from sepomex.models import State, Municipality, PostalCode, Settlement
from sepomex.models import SettlementType, Zone


class StateResource(ModelResource):
    class Meta:
        queryset = State.objects.all()
        resource_name = 'state'


class MunicipalityResource(ModelResource):

    state = fields.ForeignKey(StateResource, 'state')

    class Meta:
        queryset = Municipality.objects.all()
        resource_name = 'municipality'


class PostalCodeResource(ModelResource):
    class Meta:
        queryset = PostalCode.objects.all()
        resource_name = 'postal_code'
        filtering = {
            'number': ALL_WITH_RELATIONS,
        }


class SettlementTypeResource(ModelResource):
    class Meta:
        queryset = SettlementType.objects.all()
        resource_name = 'settlement_type'


class ZoneResource(ModelResource):
    class Meta:
        queryset = Zone.objects.all()
        resource_name = 'zone'


class SettlementResource(ModelResource):

    municipality = fields.ForeignKey(MunicipalityResource, 'municipality')
    postal_code = fields.ForeignKey(PostalCodeResource, 'postal_code')
    type = fields.ForeignKey(SettlementTypeResource, 'type')
    zone = fields.ForeignKey(ZoneResource, 'zone')

    class Meta:
        queryset = Settlement.objects.all()
        resource_name = 'settlement'
        filtering = {
            'postal_code': ALL_WITH_RELATIONS,
        }
